import numpy as np

def find_two_smallest(aa):
    i1, i2 = -1, -1
    a1, a2 = np.inf, np.inf
    for i in range(0, len(aa)):
        if aa[i] < a1:
            i2, a2 = i1, a1
            i1, a1 = i, aa[i]
        elif aa[i] < a2 and aa[i] != a1:
            i2, a2 = i, aa[i]
    return i1, i2

def tsk0_unpack_parameters(x, k, n):
    kn = k * n
    a = x[       :     kn].reshape((k, n))
    c = x[    kn : 2 * kn].reshape((k, n))
    b = x[2 * kn :       ]
    return a, c, b

def tsk0_pack_parameters(a, c, b):
    k, n = a.shape
    kn = k * n
    x = np.zeros(2 * kn + k)
    x[       :     kn] = a.reshape(kn)
    x[    kn : 2 * kn] = c.reshape(kn)
    x[2 * kn :       ] = b.reshape(k)
    return x

def compute_gaussians(x, a, c, sum_axis=1):
    w = (c - x) ** 2 / (2 * a)
    return np.exp(-np.sum(w, sum_axis))

def cutoff_number(x, a, b):
    if x < a:
        return a
    elif x > b:
        return b
    return x
