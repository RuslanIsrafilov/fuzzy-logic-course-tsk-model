import numpy as np
import numpy.linalg as linalg
from . import utils

class DefaultClusteringMethod(object):
    def __init__(
            self, initial_centroids, max_epoch=10, accuracy=1e-4,
            winner_correction=0.2, loser_correction=0.02):
        self.initial_centroids = initial_centroids
        self.centroids = None
        self.max_epoch = max_epoch
        self.accuracy = accuracy
        self.winner_correction = winner_correction
        self.loser_correction = loser_correction
        self.achived_accuracy = float('inf')

    def fit(self, data):
        self._k = len(self.initial_centroids)
        self._weights = np.ones((self._k))
        self._data = np.array(data)
        self.centroids = np.array(self.initial_centroids, copy=True)
        self.w_correction = self.winner_correction
        self.r_correction = self.loser_correction
        for num in range(0, self.max_epoch):
            prev_centroids = np.array(self.centroids, copy=True)
            self._fit_epoch(num)
            if self._check_stop_condition(prev_centroids, self.centroids):
                break

    def _fit_epoch(self, num):
        f = (num + 1) / self.max_epoch
        for x in self._data:
            self._fit_epoch_object(x)
        self.w_correction -= f * self.w_correction
        self.r_correction -= f * self.r_correction

    def _fit_epoch_object(self, x):
        dists = [self._compute_norm(x, k) for k in range(0, self._k)]
        w, r = utils.find_two_smallest(dists)
        cw = self.centroids[w]
        cr = self.centroids[r]
        self._weights[w] += 1
        self.centroids[w] += self.w_correction * (x - cw)
        self.centroids[r] -= self.r_correction * (x - cr)

    def _compute_norm(self, x, k):
        c = self.centroids[k]
        f = self._weights[k] / sum(self._weights)
        return f * linalg.norm(np.array(x) - np.array(c))

    def _check_stop_condition(self, c0, c1):
        s = 0
        for i in range(0, self._k):
            s += linalg.norm(c0[i] - c1[i])
        acc = s / self._k
        self.achived_accuracy = acc
        return acc < self.accuracy
