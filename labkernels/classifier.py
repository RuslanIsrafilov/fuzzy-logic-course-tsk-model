import numpy as np
import numpy.random as random
from . import clustering
from . import optimization
from . import utils

class FuzzyClassifier(object):
    def __init__(self, initial_centroids, **kwargs):
        self.initial_centroids = initial_centroids
        self.params = kwargs
        self.clustering_centroids = None
        self.clustering_achived_accuracy = None
        self.best_parameter = None
        self.best_value = None

    def fit(self, data, labels, a=None, c=None, b=None):
        self._clustering_stage(data, labels, a, c, b)
        enable_optimization = self.params.get('enable_optimization', True)
        if enable_optimization:
            self._optimization_stage(data, labels)

    def predict(self, data):
        if len(data.shape) == 1:
            data = data.reshape((1, data.shape[0]))
        k, n = self.clustering_centroids.shape
        a, c, b = self.best_a, self.best_c, self.best_b
        classes = np.zeros(data.shape[0], dtype=np.int)
        for i in range(0, data.shape[0]):
            w = utils.compute_gaussians(data[i], a, c)
            cls = int(round(np.dot(w, b) / np.sum(w)))
            classes[i] = utils.cutoff_number(cls, 0, 2)
        return classes

    def score(self, test_data, test_labels):
        predicted = self.predict(test_data)
        hits = np.equal(test_labels, predicted).astype(np.int)
        return np.sum(hits) / len(test_labels) * 100.0

    def validate(self, data, labels, folds=5, shuffle=False, a=None, c=None, b=None):
        if shuffle:
            permutation = random.permutation(data.shape[0])
            data = data[permutation]
            labels = labels[permutation]
        N = data.shape[0]
        fold_size = int(N / folds)
        score = 0
        for i in range(0, N, fold_size):
            end_i = i + fold_size
            data_before, labels_before = data[:i], labels[:i]
            data_after, labels_after = data[end_i:], labels[end_i:]
            trainig_data = np.vstack((data_before, data_after))
            trainig_labels = np.hstack((labels_before, labels_after))
            test_data, test_labels = data[i:end_i], labels[i:end_i]
            self.fit(trainig_data, trainig_labels, a=a, c=c, b=b)
            score += self.score(test_data, test_labels)
        return score / folds

    def _clustering_stage(self, data, labels, a, c, b):
        clustering_max_epoch         = self.params.get('clustering_max_epoch', 10)
        clustering_accuracy          = self.params.get('clustering_accuracy', 1e-4)
        clustering_winner_correction = self.params.get('clustering_winner_correction', 0.2)
        clustering_loser_correction  = self.params.get('clustering_loser_correction', 0.02)
        progress_callback            = self.params.get('clustering_progress_callback', None)
        if c is None or a is None:
            clusterizer = clustering.DefaultClusteringMethod(
                self.initial_centroids, clustering_max_epoch, clustering_accuracy,
                clustering_winner_correction, clustering_loser_correction)
            clusterizer.fit(data)
            self.clustering_centroids = self._filter_outlayers(clusterizer.centroids)
            self.clustering_achived_accuracy = clusterizer.achived_accuracy
        else:
            self.clustering_centroids = c
        if progress_callback:
            progress_callback(self.clustering_centroids, self.clustering_achived_accuracy)
        if a is None:
            a = self._generate_a(self.clustering_centroids)
        if c is None:
            c = self._generate_c(self.clustering_centroids)
        if b is None:
            b = self._generate_b(data, labels, a, c)
        self.best_parameter = utils.tsk0_pack_parameters(a, c, b)
        self.best_a = a
        self.best_c = c
        self.best_b = b

    def _optimization_stage(self, data, labels):
        k, n = self.clustering_centroids.shape
        swarm = self._generate_initial_swarm(data, labels)
        objective = optimization.ObjectiveFunctionTSK0(data, labels, k)
        pso_momentum          = self.params.get('pso_momentum', 0.5)
        pso_local_attraction  = self.params.get('pso_local_attraction', 2)
        pso_global_attraction = self.params.get('pso_global_attraction', 3)
        pso_iterations        = self.params.get('pso_iterations', 100)
        pso_accuracy          = self.params.get('pso_accuracy', 1e-4)
        progress_callback     = self.params.get('pso_progress_callback', None)
        pso_positions_corrector = optimization.PositionsCorrectorTSK0(k, n)
        pso = optimization.ParticleSwarmOptimizationMethod(
            objective, swarm, pso_momentum, pso_local_attraction, pso_global_attraction,
            pso_iterations, pso_accuracy, pso_positions_corrector, progress_callback)
        pso.fit()
        self.best_parameter = pso.minimizer
        self.best_value = pso.minimum
        a, c, b = utils.tsk0_unpack_parameters(self.best_parameter, k, n)
        self.best_a = a
        self.best_c = c
        self.best_b = b

    def _generate_initial_swarm(self, data, labels):
        a = self._generate_a(self.clustering_centroids)
        c = self._generate_c(self.clustering_centroids)
        b = self._generate_b(data, labels, a, c)
        k, n = c.shape
        kn = k * n
        dimension = 2 * kn + k
        swarm_size    = self.params.get('swarm_size', 500)
        initial_a_max = self.params.get('initial_a_max', 10)
        initial_b_min = self.params.get('initial_b_min', -5)
        initial_b_max = self.params.get('initial_b_max', 5)
        b_variance = initial_b_max - initial_b_min
        swarm = np.zeros((swarm_size, dimension))
        swarm[:, : kn       ] = initial_a_max * random.random((swarm_size, kn))
        swarm[:, kn : 2 * kn] = random.random((swarm_size, kn))
        swarm[:, 2 * kn :   ] = initial_b_min + b_variance * random.random((swarm_size, k))
        swarm[0, :          ] = utils.tsk0_pack_parameters(a, c, b)
        return swarm

    def _filter_outlayers(self, centroids):
        filtered = []
        for centroid in centroids:
            is_centroid_in_bounds = True
            for x in centroid:
                if x > 1 or x < 0:
                    is_centroid_in_bounds = False
                    break
            if is_centroid_in_bounds:
                filtered.append(centroid)
        return np.array(filtered)

    def _generate_a(self, centroids):
        k = centroids.shape[0]
        initial_c_r = self.params.get('initial_c_r', 1.5)
        dists = self._compute_distances_matrix(centroids)
        cluster_indices = np.argmin(dists, axis=1)
        a = np.zeros(centroids.shape)
        for i in range(0, k):
            a[i, :] = dists[i, cluster_indices[i]] / initial_c_r
        self.initial_a = a
        return a

    def _generate_c(self, centroids):
        self.initial_c = centroids
        return centroids

    def _generate_b(self, data, labels, a, c):
        k, N = a.shape[0], data.shape[0]
        w, b = np.zeros(N), np.zeros(k)
        for i in range(0, k):
            ci, ai = c[i, :], a[i, :]
            for t in range(0, N):
                w[t] = utils.compute_gaussians(data[t], ai, ci, 0)
            b[i] = np.dot(w, labels) / np.sum(w)
        self.initial_b = b
        return b

    def _compute_distances_matrix(self, centroids):
        k = centroids.shape[0]
        dists = np.full((k, k), np.inf)
        for i in range(0, k):
            for j in range(i + 1, k):
                d = centroids[i] - centroids[j]
                dists[i, j] = np.dot(d, d)
                dists[j, i] = dists[i, j]
        return dists
