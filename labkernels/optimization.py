import numpy as np
import numpy.random as random
from . import utils

class PositionsCorrectorTSK0(object):
    def __init__(self, k, n):
        self.k = k
        self.n = n

    def __call__(self, positions, velocities):
        kn = self.k * self.n
        pa, pc = positions[:, :kn], positions[:, kn : 2 * kn]
        va, vc = velocities[:, :kn], velocities[:, kn : 2 * kn]
        it = np.nditer([pa, va], op_flags=[['readwrite'], ['readonly']])
        for x, v in it:
            i = 0
            while x < 0 and i < 20:
                i += 1
                x[...] -= v / (2 ** i)
        it = np.nditer([pc, vc], op_flags=[['readwrite'], ['readonly']])
        for x, v in it:
            i = 0
            while x <= 0 and x >= 1 and i < 20:
                i += 1
                x[...] -= v / (2 ** i)


class ObjectiveFunctionTSK0(object):
    def __init__(self, data, labels, k):
        self.k = k
        self.data = data
        self.labels = labels

    def __call__(self, x):
        k, n = self.k, self.data.shape[1]
        a, c, b = utils.tsk0_unpack_parameters(x, k, n)
        s = 0
        for t in range(0, len(self.labels)):
            w = utils.compute_gaussians(self.data[t], a, c)
            s += (np.dot(w, b) / np.sum(w) - self.labels[t]) ** 2
        return s / len(self.labels)


class ParticleSwarmOptimizationMethod(object):
    def __init__(
            self, objective, initial_swarm, momentum=0.2, local_attraction=2,
            global_attraction=3, iterations=100, accuracy=1e-4, positions_corrector=None,
            progress_callback=None):
        self.objective = objective
        self.initial_swarm = initial_swarm
        self.momentum = momentum
        self.local_attraction = local_attraction
        self.global_attraction = global_attraction
        self.iterations = iterations
        self.accuracy = accuracy
        self.positions_corrector = positions_corrector
        self.progress_callback = progress_callback

    def fit(self):
        self._initialize()
        self._update_best_positions()
        for i in range(0, self.iterations):
            self._compute_next_velocities()
            self._compute_next_positions()
            self._update_best_positions()
            if self.progress_callback:
                self.progress_callback(i, {
                    'positions': self._positions,
                    'velocities': self._velocities,
                    'values': self._values,
                    'minimizer': self._global_position,
                    'minimum': self._global_value,
                    'value_change': self._last_global_change
                })
            if self._last_global_change < self.accuracy:
                break
        self.positions = self._positions
        self.values = self._values
        self.minimizer = self._global_position
        self.minimum = self._global_value

    def _initialize(self):
        x = self.initial_swarm
        self._positions = np.array(x, copy=True)
        self._velocities = np.zeros(x.shape)
        self._local_positions = np.zeros(x.shape)
        self._global_position = np.zeros(x.shape[1])
        self._values = np.zeros(x.shape[0])
        self._local_values = np.full(x.shape[0], np.inf)
        self._global_value = np.inf
        self._compression = self._compute_compression_factor()

    def _compute_next_velocities(self):
        gp = self._global_position
        lp = self._local_positions
        p = self._positions
        v = self._velocities
        r1, r2 = random.random(), random.random()
        l, g = r1 * self.local_attraction, r2 * self.global_attraction
        v[...] = self._compression * (self.momentum * v + l * (lp - p) + g * (gp - p))

    def _compute_next_positions(self):
        self._positions += self._velocities
        if self.positions_corrector:
            self.positions_corrector(self._positions, self._velocities)

    def _update_best_positions(self):
        p = self._positions
        for i in range(0, p.shape[0]):
            value = self.objective(p[i])
            self._values[i] = value
            if value < self._local_values[i]:
                self._local_values[i] = value
                self._local_positions[i, :] = p[i]
            if value < self._global_value:
                self._last_global_change = np.abs(self._global_value - value)
                self._global_value = value
                self._global_position[:] = p[i]

    def _compute_compression_factor(self):
        phi = self.local_attraction + self.global_attraction
        return 2 / np.abs(2 - phi - np.sqrt(phi ** 2 - 4 * phi))
